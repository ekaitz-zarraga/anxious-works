#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Anxious'
SITENAME = u'Anxious Works'
SITESUBTITLE = u''
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'www'
STATIC_PATHS = ('static',)
PAGE_PATHS = ('pages',)
#ARTICLE_PATHS = ('',)

TIMEZONE = 'Europe/Madrid'

DEFAULT_LANG = u'en'
LOCALE = ('en_US.UTF8',)


# Category folders
USE_FOLDER_AS_CATEGORY = False

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 20

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Tipography improvements
TYPOGRIFY = True

# Theme
THEME = 'themes/anxious'

# Menu
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_PAGES_ON_MENU = True
MENUITEMS = [ ]

# Tags
# Doesn't work with links correctly :S
#TAGS_SAVE_AS = 'series.html'
#TAG_SAVE_AS  = 'series/{slug}.html'

# Markdown extras
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.footnotes': {},
        'markdown.extensions.tables': {},
        # optionally, more extensions,
        # e.g. markdown.extensions.meta
    },
    'output_format': 'html5',
}
