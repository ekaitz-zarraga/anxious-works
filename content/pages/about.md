Title: About

<style>
.profile{
    float: right;
}
@media only screen and (max-width: 600px){
    .profile {
        float: none;
        text-align: center;
    }
}

/* SVG Animation testing for my face */
@keyframes eye-pulse{
  0%, 10%, 100% {
    transform: scale(1.05);
    transform-origin: 32.7px 52.5px;
  }
  5% {
    transform: scale(0.9);
    transform-origin: 32.7px 52.5px;
  }
}
.face .eye-left{
  animation-name: eye-pulse;
  animation-duration: 1.5s;
  animation-timing-function: ease-in-out;
  animation-iteration-count: infinite;
  animation-play-state: running;
}
</style>

<div class="profile">
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   width="250"
   height="350"
   viewBox="0 0 66.146 92.604"
   class="inline-block face"
   version="1.1"
   id="svg46"
   sodipodi:docname="_face.svg"
   inkscape:version="0.92.4 (5da689c313, 2019-01-14)">
  <defs
     id="defs10">
    <!--Pattern stripes-->
    <pattern
       id="a"
       patternTransform="scale(10)"
       height="1"
       width="2"
       patternUnits="userSpaceOnUse">
      <path
         d="M0-.5h1v2H0z"
         id="path2" />
    </pattern>
    <pattern
       xlink:href="#a"
       id="c"
       patternTransform="translate(30 40) scale(.4)" />
    <!--Pattern waves -->
    <pattern
       id="b"
       height="5"
       width="30"
       patternUnits="userSpaceOnUse">
      <path
         d="M7.597.061C5.079-.187 2.656.302-.01 1.788v1.273c2.783-1.63 5.183-2.009 7.482-1.781 2.298.228 4.497 1.081 6.781 1.938 4.567 1.713 9.551 3.458 15.813-.157l-.004-1.273c-6.44 3.709-10.816 1.982-15.371.273-2.278-.854-4.576-1.75-7.094-2z"
         id="path6" />
    </pattern>
    <pattern
       patternTransform="translate(-35.5 -6) scale(.265)"
       id="e"
       xlink:href="#b" />
  </defs>
  <g
     class="waved-shadow"
     id="g1651">
    <path
       id="path30"
       fill="url(#e)"
       d="M15.1 53.18c.74-2.768 2.294-4.32 1.587-9.26-1.613-2.456-2.89-5.474-3.704-9.26.76-3.297.029-7.544 3.445-9.151 8.819-.615 16.84.344 25.659 2.536 6.286-6.302 8.973-8.153 14.023-8.197C48.885 5.426 38.332 4.313 27.723 3.428 14.973 5.714 5.489 17.224 4.805 21.17c-1.154 8.995 1.09 20.917 8.788 27.56z" />
    <path
       id="path32"
       fill="url(#e)"
       d="M16.025 57.017c.927.66 2.779 3.44 2.779 3.44l1.587 4.233 5.689 1.984c4.154 3.925 6.938 7.85 8.599 11.774l3.207 2.38c6.9.659 12.65 1.048 13.594.266.389-.446 3.836-2.778 3.836-2.778l3.043-4.498c.446-1.018.775-1.992-.737-2.275-2.738 1.415-5.746 3.066-7.517 3.636-1.317.334-6.54.912-9.822 1.369-1.607.753-2.285 2.282-2.397 4.28l-.878-3.15c.234-1.51 1.369-2.423 2.31-3.463 6.716-3.153 10.608-.745 17.592-5.376 1.03-.731 2.302-.123 3.582.522l-4.228 14.535c-7.183 8.695-12.021 5.372-17.31 4.36-5.693-4.21-11.393-8.376-16.843-14.041-3.124-5.368-4.36-11.365-6.085-17.198z" />
  </g>
  <g
     class="lined-shadow"
     id="g1656">
    <path
       id="path18"
       fill="url(#c)"
       d="M23.192 51.27c.113-.14 3.268-3.971 4.298-3.971 2.46-.548 9.31-.21 12.006 1.028.394.127.147-2.052-.607-2.43-3.9-.927-6.541-1.322-10.932-1.635-4.637 2.438-3.445 4.68-4.765 7.008z" />
    <path
       id="path20"
       fill="url(#c)"
       d="M51.502 44.87c.56-1.73 1.028-2.71 1.028-2.71 2.3-1.817 3.887-3.712 5.84-5.045 1.074-.748 1.54-.374 1.54-.374l-.093 1.962-1.027.187c-2.461 1.75-4.833 3.923-7.288 5.98z" />
    <path
       id="path36"
       fill="url(#c)"
       d="M15.29 59.216l-1.627-3.81c.026-.713.11-1.597 1.437-2.226l-3.105-2.452c-1.204.637-1.977 2.495-.37 4.395 1.166 1.753 2.428 2.839 3.665 4.093z" />
  </g>
  <g
     class="outline"
     id="g931">
    <path
       id="path28"
       stroke-width=".265"
       stroke="#000"
       fill="none"
       d="M34.73 49.26c1.028.029 1.859-.031 3.13.094 1.795.177 3.563.626 5.07 2.313 1.422 1.806 1.84 3.612 2.219 5.419" />
    <path
       style="fill:none;stroke:#000000;stroke-width:0.26499999"
       inkscape:connector-curvature="0"
       id="path12"
       d="M 14.245,49.674 C 7.568,42.537 4.379,34.47 4.732,23.114 c 0.31,-7.175 11.91,-18.45 22.99,-19.687 23.725,0.557 30.688,15.537 32.24,32.637 -1.093,15.329 2.236,13.953 0.53,33.297 -1.3,4.954 -2.586,9.923 -4.228,14.535 -5.346,6.973 -11.267,6.455 -17.31,4.36 C 33.796,84.192 29.21,81.463 23.098,75.175 18.522,69.113 16.9,58.619 14.246,49.674 Z" />
    <path
       style="fill:none;stroke:#000000;stroke-width:0.26499999"
       inkscape:connector-curvature="0"
       id="path16"
       d="M 14.245,49.674 C 11.548,47.243 8.747,46.173 7.53,46.333 c -2.667,1.418 -0.57,4.947 2.294,7.429 -0.012,1.043 0.304,1.978 0.66,2.898 0.569,0.71 1.282,1.04 2.068,1.182 0.612,3.799 2.947,4.84 5.393,5.705" />
    <path
       style="fill:none;stroke:#000000;stroke-width:0.26499999"
       inkscape:connector-curvature="0"
       id="path22"
       d="m 27.957,54.494 c -0.24,-1.05 -0.037,-2.174 0.888,-3.13 1.145,-1.267 3.12,-1.958 5.886,-2.103 1.924,0.102 3.5,1.717 5.092,3.55 -0.98,0.305 -0.916,-0.308 -2.43,0.84 -0.93,1.03 -1.607,1.738 -2.662,2.197 -1.31,0.57 -2.87,0.927 -4.251,0.56 -1.02,-0.27 -1.797,-1.141 -2.523,-1.915 z m 21.489,-4.72 c 0,-0.186 0.56,-2.522 0.56,-2.522 l 4.279,-3.424 c 0.85,0.035 1.494,-0.134 2.833,0.39 0,0 0.695,1.937 0.066,3.05 -0.795,1.073 -1.83,1.81 -3.02,2.126 -1.65,0.437 -3.568,0.08 -4.718,0.38 z" />
    <path
       style="fill:none;stroke:#000000;stroke-width:0.26499999;stroke-linecap:round"
       inkscape:connector-curvature="0"
       id="path26"
       d="m 50.007,47.252 c -0.585,0.677 -2.037,1.052 -1.822,2.43 0.215,0.87 0.39,1.294 0.934,2.615 0.663,1.44 5.864,10.612 6.117,12.235 0.363,1.327 0.076,2.454 -0.09,3.649 -0.831,2.363 -2.158,2.576 -3.364,3.317 -1.236,0.287 -2.351,-0.157 -3.457,-0.654 -1.516,-0.78 -3.05,-1.718 -4.438,-1.215 -0.506,0.147 0.737,1.13 0.514,1.12 -0.947,-0.034 -2,-0.955 -2.488,-1.517 -0.342,-0.394 -0.804,-1.405 -0.69,-2.78 0.327,-1.453 1.427,-1.764 2.25,-2.596 m -2.67,14.462 c -0.576,0.198 7.18,-1.905 9.81,-0.934 2.318,-0.751 0.147,-0.664 6.915,-4.625" />
    <path
       style="fill:none;stroke:#000000;stroke-width:0.26499999"
       inkscape:connector-curvature="0"
       id="path34"
       d="m 54.352,43.795 1.402,-1.031 c 0.776,-0.135 1.435,-0.123 1.836,0.212 M 15.1,53.18 C 11.974,50.564 9.153,48.104 6.29,48.103" />
    <path
       style="fill:none;stroke:#000000;stroke-width:0.26499999"
       inkscape:connector-curvature="0"
       id="path38"
       d="m 37.394,53.652 c 0.604,-0.83 0.913,-1.628 1.16,-2.284 m 12.338,-1.824 c -0.587,-0.472 -0.865,-1.253 -0.885,-2.292" />
  </g>
  <g
     class="eye-left"
     id="g1660">
    <circle
       id="circle24"
       stroke-linejoin="round"
       stroke-width=".265"
       stroke="#000"
       fill="none"
       r="2.319"
       cy="52.5"
       cx="32.7" />
    <path
       id="path40"
       d="M32.762 51.412a1.088 1.088 0 0 1 1.078.847 1.088 1.088 0 0 1-.606 1.229 1.088 1.088 0 0 1-1.328-.34 1.088 1.088 0 0 1 .056-1.368l.817.72z" />
  </g>
  <g
     class="eye-right"
     id="g1664">
    <path
       id="path42"
       stroke-linejoin="round"
       stroke-width=".265"
       stroke="#000"
       fill="none"
       d="M53.27 44.665a2.32 2.32 0 0 1 2.136 1.584 2.32 2.32 0 0 1-.756 2.55 2.32 2.32 0 0 1-2.653.164 2.32 2.32 0 0 1-1.065-2.436" />
    <path
       id="path44"
       d="M53.19 45.85a1.088 1.088 0 0 1 1.077.848 1.088 1.088 0 0 1-.606 1.229 1.088 1.088 0 0 1-1.328-.34 1.088 1.088 0 0 1 .056-1.368l.817.72z" />
  </g>
</svg>
</div>

Since I was a child[^child] I had the weird ability to learn different kind of
crafts I feel unable to list now. That made me an engineer, but also made me a
miniature sculptor while I was studying to be an engineer, funny huh?

Since I was a child I was unable to stop.

Now I'm getting older and after months of therapy I have a name to what I was
suffering: high functioning anxiety.

Last years have been an ocean of pain for me –physically and mentally
speaking. I don't want to be ashamed of talking about it, there's no reason for
that, this have been so deeply inserted in my personality and in my way to
experiment with my reality that it have been able to rule everything around me.
It removed many beautiful moments from my life, it destroyed my health and many
of my human relationships. It broke me in way that is unrepairable.

That's an expensive price to pay.

All those issues and negativity had a counterpart: I have been an addict to
that kind of life so much time that is normal for me. My illness made me a full
time learner. Therapy, self control and being surrounded by people with a great
heart gave me some balance.

Nowadays I'm not only able to learn whatever I want to, now I'm able to enjoy
the process and start it or stop it deliberately. I'm starting to control it.
Even if it's a chimpanzee, there's someone driving this knowledge crashing
bulldozer. Sometimes the chimpanzee gets distracted and accidentally crushes a
mailbox, but I guess it's part of the process –it's learning to focus, don't be
so hard on it.

At times I remember all the pain this carried and I feel bad. Or all the pain
comes again because the chimpanzee is playing with a shotgun instead of making
what it's supposed to do. This place is a remainder of all the other parts.
This place is going to make me feel better when I'm crying because all the
things I've lost, reminding me all the things that I got back.

All the projects shared in this website, even the website itself, are the
result of the aggressive procrastination produced by anxiety, which now, for
me, is something I enjoy as part of my life. It has led me to so many fantastic
places I can't remember them all.

This website is going to remind you all who deal with anxiety there's nothing
to be ashamed of.

It was an expensive price to pay, but I'm the result of my experience
independently if it's good or bad as you are the result of yours and, like it
or not, our experience is not something we can choose. Thankfully, we can, with
effort, decide to accept it –and like it– as it is.

[^child]: It wasn't that long ago. I can still remember it.
