Title:
Status: hidden
save_as: index.html

So yeah, this is Anxious Works' website where I'll upload [my random thoughts
and philosophical dissertations]({category}thoughts), my [DIY
stuff]({category}doing) that may involve [considerable quantities of
madness](/pages/about.html) and probably some other things I'm unable to
categorize right now because I'm writing this intro just to publish the website
because I think I did a good job with [its design][design] and I want to show
it to my friends[^friends].

[design]: https://gitlab.com/ekaitz-zarraga/anxious-works/-/tree/master/themes/anxious


[^friends]: [tHIs lIsT Is iNCoMPlEte You cAn heLP bY eXPaNding It](/pages/contact.html)
